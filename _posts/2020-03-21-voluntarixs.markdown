---
layout: post
title:  "día 8 - voluntarixs"
date:   2020-03-21
image: /corona/assets/images/voluntarixs.jpeg
---
Papeles pegados en los edificios de un lugar de una ciudad de España. Queremos ayudar, sentimos que podemos hacerlo o que debemos. Algunas se organizan y deciden publicarlo como un grupo, entonces, vienen los pasos: decidir que número va en la hoja, revisar el texto, imprimir, esperar la llamada.

La comunidad es una elección y una forma, entonces las voluntarias son familia. Entonces en el barrio tienes vecinas, cercanas, y no conocidas de un par de vistas. Entonces, no es caridad, es apoyo.
