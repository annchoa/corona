---
layout: post
title: "Día 10: Limpieza y organización"
date: 2020-03-23
image: /corona/assets/images/day10.jpg
---
Esta situación inesperada, ha hecho que en el lugar donde normalmente vivo solo, tenga que compartirlo con otra persona. Hoy nos hemos dedicado a preparar un espacio compartido donde podamos trabajar y descansar con espacio para los dos.
