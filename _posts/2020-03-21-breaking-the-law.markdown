---
layout: post
title: "Día 8: Breaking the law"
date: 2020-03-21
image: /corona/assets/images/day8.jpg
---
A pesar de la prohibición de los bares (y de subir a las azoteas), hoy hemos tenido nuestra visita a nuestro "bar".

He tenido una llamada con la familia, mi primo que trabaja en el hospital dice que la UCI de su hospital ya está al 50%. A parte, ya hay dos alas completas destinadas a pacientes del virus. Dice que lo peor llegará en unas dos semanas.
